package net.tncy.cre.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Pattern;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public void initialize(ISBN constraintAnnotation) {

    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintValidatorContext) {
        boolean valid = false;
        valid = Pattern.matches("^[0-9]{13}$", bookNumber);
        return valid;
    }
}
